import platform
import psutil
import datetime
import socket
import os


def get_hardware_info():
    # Cores
    logical_cores = psutil.cpu_count()
    physical_cores = psutil.cpu_count(logical=False)
    # Disk partitions
    partitions = get_fs_info()
    return {
        'cpu': {
            'cores': {
                'logical': logical_cores,
                'physical': physical_cores,
            },
        },
        'ram': {
            'total': psutil.virtual_memory().total,
            'swap': psutil.swap_memory().total,
        },
        'partitions': partitions,
    }


def get_top_processes(count=10):
    # Get top {count} processes by RAM and CPU
    procs = []
    for p in psutil.process_iter():
        try:
            p_d = p.as_dict()
            procs.append(p_d)
        except psutil.Error:
            continue
    cpu_top_N = sorted(procs, key=lambda p: p['cpu_percent'], reverse=True)[:count]
    ram_top_N = sorted(procs, key=lambda p: p['ram_percent'], reverse=True)[:count]
    return {
        'cpu_top': cpu_top_N,
        'ram_top': ram_top_N,
    }


def get_processes():
    procs = []
    for p in psutil.process_iter():
        try:
            p_d = p.as_dict()
            procs.append(p_d)
        except psutil.Error:
            continue
    return procs


def get_performance_info():
    # CPU times
    cpu_times = [t._asdict() for t in psutil.cpu_times_percent(interval=1, percpu=True)]
    # CPU stats
    cpu_stats = psutil.cpu_stats()._asdict()
    cpu_freq = [f._asdict() for f in psutil.cpu_freq(percpu=True)]
    # Memory
    v_memory = psutil.virtual_memory()._asdict()
    swap = psutil.swap_memory()._asdict()
    # Disk
    partitions = psutil.disk_partitions()
    disk_usage = {}
    for p in partitions:
        disk_usage[p.device] = psutil.disk_usage(p.mountpoint)
    return {
        'cpu': {
            'times': cpu_times,
            'stats': cpu_stats,
            'frequency': cpu_freq,
        },
        'memory': {
            'virtual': v_memory,
            'swap': swap,
        },
        'disk': disk_usage,
    }


def get_general_info():
    uname = platform.uname()
    return uname._asdict()


def get_fs_info():
    return [p._asdict() for p in psutil.disk_partitions()]


def get_services_info():
    if os.name != 'nt':
        raise OSError('Platform not supported (Windows only)')
    services = [service._asdict() for service in psutil.win_service_iter()]
    return services


def get_sensors_info():
    temperatures = psutil.sensors_temperatures()
    temperatures_dict = {}
    for group, temps in temperatures.items():
        temperatures_dict[group] = []
        for temp in temps:
            temperatures_dict[group].append({
                'label': temp.label,
                'current': temp.current,
                'high': temp.high,
                'critical': temp.critical,
            })

    battery = psutil.sensors_battery()
    try:
        battery = battery._asdict()
        if battery.get('power_plugged', False):
            del battery['secsleft']
    except Exception:
        battery = {}

    # fans = psutil.sensors_fans()
    return {
        'temperatures': temperatures_dict,
        'battery': battery,
        # 'fans': fans,
    }


def get_envvars():
    return dict(os.environ)


def get_network_info():
    duplex_map = {
        psutil.NIC_DUPLEX_FULL: "full",
        psutil.NIC_DUPLEX_HALF: "half",
        psutil.NIC_DUPLEX_UNKNOWN: "unknown",
    }

    af_map = {
        socket.AF_INET: 'IPv4',
        socket.AF_INET6: 'IPv6',
        psutil.AF_LINK: 'MAC',
    }

    if_stats = psutil.net_if_stats()
    io_counters = psutil.net_io_counters(pernic=True)
    net_interfaces = {}

    for nic, addrs in psutil.net_if_addrs().items():
        if_info = {}

        if nic in if_stats:
            stats = if_stats[nic]
            if_info['stats'] = {
                'speed': stats.speed,
                'duplex': duplex_map[stats.duplex],
                'mtu': stats.mtu,
                'up': stats.isup,
            }

        if nic in io_counters:
            io = io_counters[nic]
            if_info['io_counters'] = {
                'incoming': {
                    'bytes': io.bytes_recv,
                    'packets': io.packets_recv,
                    'errs': io.errin,
                    'drops': io.dropin,
                },
                'outgoing': {
                    'bytes': io.bytes_sent,
                    'packets': io.packets_sent,
                    'errs': io.errout,
                    'drops': io.dropout,
                },
            }

        if_info['addresses'] = []
        for addr in addrs:
            address_dict = {
                'family': af_map.get(addr.family, addr.family),
                'address': addr.address,
            }
            if addr.broadcast:
                address_dict['broadcast'] = addr.broadcast
            if addr.netmask:
                address_dict['netmask'] = addr.netmask
            if addr.ptp:
                address_dict['ptp'] = addr.ptp
            if_info['addresses'].append(address_dict)

        net_interfaces[nic] = if_info

    connections = {}
    for conn_type in ['inet', 'tcp', 'udp', 'unix']:
        connections[conn_type] = prepare_connections_list(psutil.net_connections(conn_type))

    return {
        'interfaces': net_interfaces,
        'connections': connections,
    }


def prepare_connections_list(connections):
    for i in range(len(connections)):
        connections[i] = connections[i]._asdict()
        connections[i]['family'] = connections[i]['family'].name
        if type(connections[i]['type']) != int:
            connections[i]['type'] = connections[i]['type'].name
        if type(connections[i]['laddr']) == 'tuple':
            connections[i]['laddr'] = connections[i]['laddr'][0] + ':' + str(connections[i]['laddr'][1])
        if type(connections[i]['raddr']) == 'tuple':
            connections[i]['raddr'] = connections[i]['raddr'][0] + ':' + str(connections[i]['raddr'][1])
    return connections


def get_uptime():
    return (datetime.datetime.now() - datetime.datetime.fromtimestamp(psutil.boot_time())).total_seconds()


def get_current_users():
    return [x._asdict() for x in psutil.users()]
