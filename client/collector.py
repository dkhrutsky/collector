from . import info
from . import packer
import time
import logging
import asyncio


HOST, PORT = 'localhost', 4242
ONE_HOUR = 3600
ONE_MINUTE = 60
SOCKET_TIMEOUT = 10.0

GENERAL_PERIOD = 3600
PERFORMANCE_PERIOD = 5
SENSORS_PERIOD = 30
PROCESSES_PERIOD = 60
NETWORK_PERIOD = 60

log = logging.getLogger(__name__)
clients = {}


def make_connection(host, port):

    task = asyncio.Task(handle_client(host, port))

    clients[task] = (host, port)

    def client_done(task):
        del clients[task]
        log.info("Client Task Finished")
        if len(clients) == 0:
            log.info("Clients is empty, stopping loop.")
            loop = asyncio.get_event_loop()
            loop.stop()

    log.info("New Client Task")
    task.add_done_callback(client_done)


async def handle_client(host, port):
    log.info("Connecting to {}:{}".format(host, port))
    client_reader, client_writer = await asyncio.open_connection(host, port)
    log.info("Connected")
    try:
        # looking for token
        data = await asyncio.wait_for(client_reader.readline(),
                                      timeout=SOCKET_TIMEOUT)

        if data is None:
            log.warning("Expected token, received None")
            return

        token = data.decode().rstrip()
        print(token)
        log.info("Received token: {}".format(token))

        # Send back token to confirm
        client_writer.write(token.encode() + b"\n")

        # Loop, make tasks
        # Send info, wait for READY
        while True:
            try:
                messages = collect_data(token)
                for message in messages:
                    # Write gzip'd data length
                    client_writer.write(len(message).to_bytes(3, 'big') + b"\n")
                    # Write gzip'd data
                    client_writer.write(message)
                    log.debug("Sent {}".format(messages.index(message)))

                time.sleep(5)

            except KeyboardInterrupt:
                log.info("Stopped sending system info")
                break

        client_writer.write("STOP\n".encode())
        data = await asyncio.wait_for(client_reader.readline(),
                                      timeout=SOCKET_TIMEOUT)

        sdata = data.decode().rstrip().upper()
        log.info("Received {}".format(sdata))
    finally:
        log.info("Disconnecting from {}:{}".format(host, port))
        client_writer.close()
        log.info("Disconnected from {}:{}".format(host, port))


def collect_data(token):
    messages = []
    messages.append(packer.compress(collect_general(token)))
    messages.append(packer.compress(collect_performance(token)))
    messages.append(packer.compress(collect_processes(token)))
    messages.append(packer.compress(collect_sensors(token)))
    messages.append(packer.compress(collect_network(token)))
    return messages


def collect_general(token):
    # Every hour
    message = {}
    message['type'] = 'general'
    message['token'] = token
    message['uname'] = info.get_general_info()
    message['hardware'] = info.get_hardware_info()
    message['fs'] = info.get_fs_info()
    message['uptime'] = info.get_uptime()
    if message['uname']['system'] == 'nt':
        message['services'] = info.get_services_info()
    return message


def collect_performance(token):
    # Every 5 seconds
    message = info.get_performance_info()
    message['type'] = 'performance'
    message['token'] = token
    return message


def collect_processes(token):
    # Every minute
    message = {
        'type': 'processes',
        'token': token,
        'processes': info.get_processes(),
    }
    return message


def collect_sensors(token):
    # Every minute
    message = info.get_sensors_info()
    message['type'] = 'sensors'
    message['token'] = token
    return message


def collect_network(token):
    # Every minute
    message = info.get_sensors_info()
    message['type'] = 'network'
    message['token'] = token
    return message


def main():
    loop = asyncio.get_event_loop()
    make_connection(HOST, PORT)
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        loop.close()
        log.info("Stopped client")


if __name__ == '__main__':
    log.setLevel(logging.DEBUG)
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)
    log.addHandler(sh)

    main()
