import gzip
import json


def compress(data):
    return gzip.compress(json.dumps(data).encode('utf8'))


def decompress(data):
    return json.loads(gzip.decompress(data).decode('utf8'))


if __name__ == '__main__':
    d = {'a': 1, 'b': 2, 'c': {'a': 3}}
    assert decompress(compress(d)) == d
