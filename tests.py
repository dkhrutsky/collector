import unittest

import client.info as info
import client.packer as packer
import client.collector as client

import server.processing as processing
import server.server as server

import platform
import os
import time
import gzip
from pymongo import MongoClient
from multiprocessing import Process


MONGO_HOST = 'localhost'
MONGO_PORT = 27000
DB_URI = 'mongodb://{}:{}/'.format(MONGO_HOST, MONGO_PORT)


class TestInfoGathering(unittest.TestCase):
    def test_hardware(self):
        data = info.get_hardware_info()
        assert set(('cpu', 'ram')) <= set(data.keys())

    def test_processes(self):
        data = info.get_processes()
        assert data
        assert data[0]['pid'] > 0

    def test_performance(self):
        data = info.get_performance_info()
        assert set(('cpu', 'memory', 'disk')) <= set(data.keys())
        assert data['cpu']
        assert data['memory'].get('virtual')

    def test_general(self):
        data = info.get_general_info()
        pl_uname = platform.uname()
        assert pl_uname.system == data['system']
        assert pl_uname.release == data['release']

    def test_services(self):
        if os.name != 'nt':
            with self.assertRaises(OSError):
                data = info.get_services_info()
        else:
            data = info.get_services_info()
            assert data

    def test_network(self):
        data = info.get_network_info()
        assert data['interfaces']
        assert data['connections']

    def test_uptime(self):
        diff = 1
        ut1 = info.get_uptime()
        time.sleep(diff)
        ut2 = info.get_uptime()
        assert ut2 > ut1
        assert round(ut2 - ut1) == diff

    def test_current_users(self):
        users = info.get_current_users()
        names = [u['name'] for u in users]
        current_user = os.environ.get('USER', os.environ.get('USERNAME'))
        assert current_user in names


class TestPacker(unittest.TestCase):
    def test_pack_unpack(self):
        data = {'a': 1, 'b': '2', 'c': [3, 4], 'd': {'e': ['f', 'g']}}
        decompressed = packer.decompress(packer.compress(data))
        assert decompressed == data


class TestProcessing(unittest.TestCase):
    @classmethod
    def setup_class(cls):
        mongo_client = MongoClient(DB_URI)
        cls.db = mongo_client.collector_db

    def test_decompress(self):
        data = b'0123456789'
        compressed = gzip.compress(data)
        decompressed = processing.decompress(compressed)
        assert data == decompressed

    def test_token(self):
        token = processing.generate_token()
        assert token
        assert type(token) == bytes
        token_str = token.decode()
        document = TestProcessing.db.tokens.find_one({'token': token_str})
        assert document
        assert document['token'] == token_str

    def test_store(self):
        data = {
            'type': 'general',
            'token': processing.generate_token(),
            'uname': info.get_general_info(),
        }
        processing.store(data.copy())
        del data['type']
        document = TestProcessing.db.general.find_one(data)
        assert document
        del document['_id']
        assert document == data

    def test_process(self):
        data = {
            'type': 'general',
            'token': str(processing.generate_token()),
            'uname': info.get_general_info(),
        }
        packed = packer.compress(data.copy())
        processing.process(packed)
        del data['type']
        document = TestProcessing.db.general.find_one(data)
        assert document
        del document['_id']
        assert document == data

    def test_process_no_token(self):
        data = {
            'type': 'general',
            'uname': {},
        }
        packed = packer.compress(data)
        with self.assertRaises(processing.InvalidMessageError):
            processing.process(packed)

    def test_process_invalid_format(self):
        data = b'1234567890'
        compressed = gzip.compress(data)
        with self.assertRaises(processing.InvalidMessageError):
            processing.process(compressed)

    def test_process_not_packed(self):
        data = b'1234567890'
        with self.assertRaises(processing.InvalidMessageError):
            processing.process(data)


class TestCommunication(unittest.TestCase):
    @classmethod
    def setup_class(cls):
        mongo_client = MongoClient(DB_URI)
        cls.db = mongo_client.collector_db

    def test_collect_and_store(self):
        server_process = Process(target=server.main)
        client_process = Process(target=client.main)

        server_process.start()
        client_process.start()
        time.sleep(5)
        client_process.terminate()
        server_process.terminate()
