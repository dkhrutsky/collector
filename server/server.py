import logging
import asyncio
from .processing import process, InvalidMessageError, generate_token

BUFFER_SIZE = 1024 * 512
HOST, PORT = 'localhost', 4242
log = logging.getLogger(__name__)
clients = {}


def accept_client(client_reader, client_writer):
    task = asyncio.Task(handle_client(client_reader, client_writer))
    clients[task] = (client_reader, client_writer)

    def client_done(task):
        del clients[task]
        client_writer.close()
        log.info("Client closed connection")

    log.info("New connection")
    task.add_done_callback(client_done)


async def handle_client(client_reader, client_writer):
    token = generate_token()
    print(token.decode())
    client_writer.write(token + b'\n')

    data = await asyncio.wait_for(client_reader.readline(),
                                  timeout=10.0)

    if data is None:
        log.warning("Expected confirmation, received None")
        return

    sdata = data.decode().rstrip()
    log.info("Received {}".format(sdata))
    if sdata != token.decode().rstrip():
        log.warning("Invalid token {}".format(sdata))
        return

    while True:
        # wait for data from client
        data = await asyncio.wait_for(client_reader.readline(),
                                      timeout=10.0)
        if data is None:
            log.warning("Received no data")
            return

        sdata = data.rstrip()
        log.debug("Received {}".format(sdata))

        if len(sdata) == 3:
            data_length = int.from_bytes(data.rstrip(), 'big')
            try:
                zipped_data = await asyncio.wait_for(client_reader.readexactly(data_length),
                                                     timeout=10.0)
                await process(zipped_data)
            except InvalidMessageError as e:
                log.warning(e)
                return
            except asyncio.IncompleteReadError as e:
                log.error("Client has provided incomplete data")
                return
        elif sdata.decode().upper() == "STOP":
            client_writer.write("STOP\n".encode())
            log.info('Received STOP message. Closing connection.')
            return
        else:
            log.warning('Client sending unneeded data')
            return


def main():
    loop = asyncio.get_event_loop()
    f = asyncio.start_server(accept_client, host=HOST, port=PORT)
    loop.run_until_complete(f)
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        loop.close()
        log.info("Stopped server")


if __name__ == "__main__":
    log.setLevel(logging.DEBUG)
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)
    log.addHandler(sh)

    main()
