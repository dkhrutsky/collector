from pymongo import MongoClient
import gzip
import json
import logging
import uuid


MONGO_HOST = 'localhost'
MONGO_PORT = 27000
DB_URI = 'mongodb://{}:{}/'.format(MONGO_HOST, MONGO_PORT)
mongo_client = MongoClient(DB_URI)
db = mongo_client.collector_db
log = logging.getLogger(__name__)


class InvalidMessageError(Exception):
    pass


def store(message):
    log.info('Got {} information'.format(message['type']))
    collection = db[message['type']]
    del message['type']
    collection.insert_one(message)


def generate_token():
    tokens = db.tokens
    token = str(uuid.uuid4())
    tokens.insert_one({'token': token})
    return token.encode('utf8')


def process(bin_message):
    try:
        data = json.loads(decompress(bin_message).decode())
        if type(data) != dict:
            raise InvalidMessageError('Invalid info data format')
    except json.JSONDecodeError:
        raise InvalidMessageError('Invalid info data format')
    except OSError:
        raise InvalidMessageError('Not gzip bytes')

    if not data.get('token'):
        raise InvalidMessageError('Cannot identify your machine. Please provide a token.')

    t = data.get('type', 'other')
    store(data)
    response = t.encode('utf8')
    return response


def decompress(data):
    try:
        data = gzip.decompress(data)
        return data
    except Exception:
        raise InvalidMessageError('Cannot decompress your message')
